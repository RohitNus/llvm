#include <cstdio>
#include <iostream>
#include <set>
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/raw_ostream.h"


/*Color Encoding*/
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"



using namespace llvm;
struct CFG{
        /* Name of the BB: can be named or unnamed */
        std::string name;
        /* Pointer to the BasiBlock pointing too */
        llvm::BasicBlock* pointer;
        /* Set of children */
        std::set<CFG*> children;
};

/* This function checks whether there is a change in the secretVariables of a BasicBlock */
bool noChange(BasicBlock*, std::set<Instruction*>);
/* This function prints the visited blocks */
void printVisitedBlocks(std::set<BasicBlock*>);
/* This function detects all the loops in the program(while loop specifically) */
void detectAllloopBlocks(BasicBlock*, std::set<BasicBlock*>);
/* Checks whether a secret variables already exists in the set */
bool secretVariableAlreadyExists(std::set<Instruction*>, Instruction*);
/* This function updates the new found secret variables */
void updateSecretVars(BasicBlock*, std::set<Instruction*>);
/* This function generates the CFG of the program*/
void generateCFG(BasicBlock*, CFG*, std::set<Instruction*>);
/* Graph operations */
void addNode(CFG* , CFG*);
/* Pretty print the CFG */
void printCFG(CFG*);
/* Print the tainted variable */
void printTainted();
/* Gets a new name or an existing name for a basicblock */
std::string getName(BasicBlock*);
/*Gets a new variable name*/
std::string getVariableName(Instruction*);
/* Gets a new CFG node */
CFG* getNode(llvm::BasicBlock*, std::string);
/* Function which marks the tainted variables */
std::set<Instruction*> checkLeakage(BasicBlock*,std::set<Instruction*>);
/* BasicBlock set*/
std::set<BasicBlock*> BBSet;
/* BasicBlock to name map*/
std::map<BasicBlock*, std::string> BBtoNameMap;
/* Varible to name map*/
std::map<Instruction*, std::string> instNameMap;
/* BasicBlock to Instruction set*/
std::map<BasicBlock*, std::set<Instruction*>> BBtoInstructionSet;
/* Stores the pointer to the loop blocks*/
std::set<BasicBlock*> loopBlocks;
int bbLabelCounter = 0;
int slotTrackerNumber = 0;
void printSecretVars(std::set<Instruction*> secretVariables);



int main(int argc, char **argv)
{
    // Read the IR file.
    LLVMContext &Context = getGlobalContext();
    SMDiagnostic Err;
    Module *M = ParseIRFile(argv[1], Err, Context);
    CFG * main;
    if (M == nullptr)
    {
      fprintf(stderr, "error: failed to load LLVM IR file \"%s\"", argv[1]);
      return EXIT_FAILURE;
    }
    
    std::set<Instruction*> secretVars;
    /* Visited node set, this is to determine the presence of loop */
     
    std::set<BasicBlock*> visitedNode;
    for (auto &F: *M) {
      /* Check to see if the function has a name main. This would be the entry point*/
      if (strncmp(F.getName().str().c_str(),"main",4) == 0){
	  BasicBlock* BB = dyn_cast<BasicBlock>(F.begin());
          /* create the root node of the CFG: main_node */
          main = getNode(BB, "main");
	  BBtoNameMap.insert(std::make_pair(BB, "main"));
          /* detects all the loop blocks and updates the loop blocks. */
          detectAllloopBlocks(BB, visitedNode);
          /* Generates the CFG as well as populates the secretVars */
	  generateCFG(BB, main, secretVars);
      }
    }
    /* Code to print the block and its tainted Vars */
    printTainted();
}

CFG* getNode(llvm::BasicBlock* BB, std::string bbName){
    /* Function to create a basic block node */
    CFG* newNode = new CFG;
    newNode->name = bbName;
    newNode->pointer = BB;
    return newNode;
}

void detectAllloopBlocks(BasicBlock* BB, std::set<BasicBlock*> visitedNode){
	/* Function is used to detect all the basic blocks with a while loop*/
        if (BBtoNameMap.find(BB) == BBtoNameMap.end()){
		BBtoNameMap.insert(std::make_pair(BB, getName(BB)));
	}
        //printVisitedBlocks(visitedNode);
        if (visitedNode.find(BB)  == visitedNode.end()){
		visitedNode.insert(BB);
                const TerminatorInst *TInst = BB->getTerminator();
        	int NSucc = TInst->getNumSuccessors();
		for (int i = 0;  i < NSucc; ++i) {
			std::set<BasicBlock*> visitedNodeToChild = visitedNode;
               		BasicBlock *Succ = TInst->getSuccessor(i);
                        detectAllloopBlocks(Succ, visitedNodeToChild);
		}
	}
	else{
		/* Found a loop, hence add it.*/
                if (loopBlocks.find(BB) == loopBlocks.end()){
                	printf("loop detected");
                        printf("%s\n", BBtoNameMap.find(BB)->second.c_str());
			loopBlocks.insert(BB);
		}
	
	}
	return;
      
}

void generateCFG(BasicBlock* BB, CFG* parentNode, std::set<Instruction*> secretVars){
        std::set<Instruction*> newSecretVars = checkLeakage(BB,secretVars);
        bool pass = true;
        if (loopBlocks.find(BB) != loopBlocks.end() && noChange(BB, newSecretVars)){
		/* A loop is detected and If there is a change in newSecretVars, then pass else Not */
		pass = false;                        
	}
        if (pass){
        	updateSecretVars(BB, newSecretVars);
		const TerminatorInst *TInst = BB->getTerminator();
  		int NSucc = TInst->getNumSuccessors();
  		for (int i = 0;  i < NSucc; ++i) {
    			BasicBlock *Succ = TInst->getSuccessor(i);
                	std::map<BasicBlock*, std::string>::iterator it;
                	std::string name;
                	it = BBtoNameMap.find(Succ);
                	if (it != BBtoNameMap.end()){
                		name = it->second;
			}
                	else{
                		name = getName(Succ);
                        	BBtoNameMap.insert(std::make_pair(Succ, name));
                	}
                	//Add the node to BBtoNameMap.
                	CFG* newNode = getNode(Succ, name);
                	addNode(parentNode, newNode);
    			generateCFG(Succ, newNode, newSecretVars);
  		}
                if (NSucc == 0){
			/* Check if sink is part of the list for the last basic block */
			for (auto var: newSecretVars){
				if (strncmp(var->getName().str().c_str(),"sink",4) == 0) {
					printf(ANSI_COLOR_RED "OMG, Source has leaked to sink"
                                		ANSI_COLOR_RESET"\n");
      				}

			}

		}
       } 
}

bool noChange(BasicBlock* BB, std::set<Instruction*> newSecretVars){
        /* Function checks the whether there is change in the secretvars for the basicblock*/
	if (BBtoInstructionSet.find(BB) == BBtoInstructionSet.end()){
		return false;
	}
	else {
		std::set<Instruction*> oldVars = BBtoInstructionSet.find(BB)->second;
		bool same = true;
		for (auto elem: newSecretVars){
			if (oldVars.find(elem) == oldVars.end()){
				same = false;
				break;
			}
		}
		return same;
	}
}


void updateSecretVars(BasicBlock* BB, std::set<Instruction*> newSecretVars){
	if (BBtoInstructionSet.find(BB) == BBtoInstructionSet.end()){
		BBtoInstructionSet.insert(std::make_pair(BB, newSecretVars));
	}
	else{
		/* Take union and update */
                printf("set union\n");
		for (auto elem: newSecretVars){
			BBtoInstructionSet.find(BB)->second.insert(elem);
		}
	}
}

std::set<Instruction*> checkLeakage(BasicBlock* BB,
				    std::set<Instruction*> secretVars){
	/*Function which checks Leakage of public to private variables*/
        for (auto &I: *BB){
                /* Check for allocate Instruction */
                if (isa<AllocaInst>(I)){
			if ((strncmp(I.getName().str().c_str(),"source",6) == 0) && !secretVariableAlreadyExists(secretVars, dyn_cast<Instruction>(&I))){
                        	//printf("Source variable discovered\n");
                        	/* Adding dangerous variable to the list */
                                secretVars.insert(dyn_cast<Instruction>(&I));
                                instNameMap.insert(std::make_pair(dyn_cast<Instruction>(&I), "source"));
       		         }

		}
                else if (isa<LoadInst>(I)){
                        //printf("Found a Load Instruction\n");
                        Instruction* labelInst = dyn_cast<Instruction>(&I);
                        Instruction* op1 = dyn_cast<Instruction>(I.getOperand(0));
                        printf("%s\n", op1->getName().str().c_str());
                        std::string name;
                        if (instNameMap.find(labelInst) == instNameMap.end()){
                        	name = getVariableName(labelInst);
                                instNameMap.insert(std::make_pair(labelInst, name));
                        }
                        if (secretVariableAlreadyExists(secretVars, op1)) {
                                //printf("Load Instruction added:  %s\n", name.c_str());
                        	secretVars.insert(labelInst);
                        }
                }
                else if (isa<StoreInst>(I)){
                        //printf("Store Instruction find\n");
			/* Found a store Instruction */
                        Value* v = I.getOperand(0);
      			Instruction* op1 = dyn_cast<Instruction>(v);
                        Instruction* op2 = dyn_cast<Instruction>(I.getOperand(1));
      			if (op1 != nullptr &&
        			secretVars.find(op1) != secretVars.end()){
                                std::string name;
                                if (instNameMap.find(op2) == instNameMap.end()){
					name = getVariableName(op2);
                                        instNameMap.insert(std::make_pair(op2, name));
                                }
                                //printf("Store Instruction added:  %s\n", name.c_str());
        			secretVars.insert(dyn_cast<Instruction>(op2));
      			}
		}
                else if (isa<CmpInst>(I)){
                        /* Do nothing in this case */
			//printf("Found a compare instruction\n");
		}
                else if (isa<BranchInst>(I)){
			/* Do nothing in this case */
                        //printf("Found a branch instruction\n");
                }
                else if (isa<BinaryOperator>(I)){
			Instruction* labelInst = dyn_cast<Instruction>(&I);
                        std::string name;
                        if (instNameMap.find(labelInst) == instNameMap.end()){
                                name = getVariableName(labelInst);
                                instNameMap.insert(std::make_pair(labelInst, name));
                        }
                        for (auto op = I.op_begin(); op != I.op_end(); op++) {
				Value* v = op->get();
				Instruction* inst = dyn_cast<Instruction>(v);
				if (inst != nullptr && secretVars.find(inst) !=
			       		secretVars.end()){
                          //      	printf("Store Instruction added:  %s\n", name.c_str());
	  				secretVars.insert(labelInst);
				}
      			}
                }
		else{
			/* Other Instruction */
		}
        }
        return secretVars;

}

void printTainted(){
         std::string name;
	for(auto it: BBtoInstructionSet){
		BasicBlock* bb = it.first;
		std::string name;
		if (bb->hasName()){
			name = bb->getName().str();
		}
                else {
			name = BBtoNameMap.find(bb)->second;
		}
                printf("%s { ", name.c_str());       
		for (auto elem: it.second){
                	if (elem->hasName()){
                        	name = elem->getName().str();
                	}
                	else{
                        	name = instNameMap.find(elem)->second;
                	}
			printf("%s ", name.c_str());
		}
                printf(" }\n");
	}
}

void printSecretVars(std::set<Instruction*> secretVariables){
	/* print the secret Variables */
        std::string name;
        for (auto elem: secretVariables){
		if (elem->hasName()){
			name = elem->getName().str();
		}
                else{
			name = instNameMap.find(elem)->second;
		}
                printf("%s ", name.c_str());
        }
}

void printVisitedBlocks(std::set<BasicBlock*> blocks){
        /* print the secret Variables */
        std::string name;
        for (auto elem: blocks){
                if (elem->hasName()){
                        name = elem->getName().str();
                }
                else{
                        name = BBtoNameMap.find(elem)->second;
                }
                printf("%s ", name.c_str());
        }
        printf("\n");
}

bool  secretVariableAlreadyExists(std::set<Instruction*> secretVars, Instruction* I){
	if (secretVars.find(I) != secretVars.end())
		return true;
        return false;
}

std::string getName(BasicBlock *BB){
	if (BB->hasName()){
        	return BB->getName().str();
        }
        else{
		return std::string("%") + std::to_string(++bbLabelCounter);
        }
}

std::string getVariableName(Instruction* I){
	if (I->hasName()){
		return I->getName().str();
	}
        else{
		return std::string("%") + std::to_string(++slotTrackerNumber);
        }
}

void printCFG(CFG *g){
	if (g == nullptr){
		return;
        }
        printf("Node Name: %s", g->name.c_str());
        for (auto elem: g->children){
        	printCFG(elem);
        }
}

void addNode(CFG* parent, CFG* child){
     /* Function to add a BasicBlock b to the children of g */
     parent->children.insert(child);
}

